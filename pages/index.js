import { client } from '../api/client'
import Product from './products/product'
export const getServerSideProps = async() => {
  const response = await client.getEntries({content_type: 'products'})
  return {
    props:{
      products: response.items
    }
  }
}
const Main = ({products}) => {
  if(products){
    return <div className="product-list">
      {
        products.map((product)=><Product key={product.sys.id} product={product} />)
      }
    </div>
  }
  return null
}
export default Main;
