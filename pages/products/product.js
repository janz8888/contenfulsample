import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const Product = ({product})=>{
  if(product){
    const { title, description, productImage } = product.fields
    return  <Card style={{ marginTop: '10px'}}> 
        <CardActionArea>
          <CardMedia
            style={{ height: '300px'}}
            image={`https:${productImage.fields.file.url}`}
            title={title}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
                {
                    description.content[0].content.map((item)=>item.value).join('')
                }
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="primary">
            Share
          </Button>
          <Button size="small" color="primary">
            More
          </Button>
        </CardActions>
      </Card>
  }
  return null
}
export default Product;