import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
 
function Footer() { 
  return (
    <footer>
      <Paper elevation={1}>
        <Typography variant="h5" component="h3">
          Product Sample
        </Typography>
        <Typography component="p">
          @2021 All right reserved
        </Typography>
      </Paper>
    </footer>
  );
}
 export default Footer;