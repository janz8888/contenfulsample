import Link from 'next/link'
import Footer from './footer'
export default function Layout({ children }) {
  return (
    <div className="layout">
      <header>
        <Link href="/">
          <a>
            <h1>
              <span>My</span>
              <span>Product</span>
            </h1>
            <h2>List</h2>
          </a>
        </Link>
      </header>

      <div className="page-content">
        { children }
      </div>
      <Footer/>
  </div>
  )
}
